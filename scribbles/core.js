///////////////////////////////////////////////////////////
// Core Scribbles
// These are the core implementation of scribbles, basically
// containing basic javascript scribbles.
///////////////////////////////////////////////////////////

/* Scribble Structure:
{
  name:             The name of the scribble, must be unique.
  description:      optional description about the scribble.
  listed:           An optional boolean value that determines
                    whether this scribble shows up in the
                    drop down suggestion box.
  params:           The expected parameters in the scribble,
                    defined as an object with name:value pairs.
  display:          A coded html string that defines how the scribble
                    block is displayed in the UI.
                    - A double space at the very start of this string
                      will be replaced with an html '&nbsp', this
                      allows there to be a space between the scribble
                      name displayed and the display defined here.
                    - Use {{SCOPE}} to include a scope where other
                      scribbles can be nested.  Any number of
                      scopes can be included and will later bee
                      distinguished via index number in an array.
                    - Wrap any parameter name in double curley
                      braces '{{}}' to have that param substituted
                      with an editable field. 
  canBeInScope:     function(scribble, scopeID) return boolean
                    An optional function that is called to validate
                    whether this scribble can be part of another
                    scribble's scope.  By default, all scopes
                    are allowed.
  canScopeContain:  function(scribble) return boolean
                    An optional function that is called to validate
                    whether a given scribble can be nested within
                    the requested scope.  By default, all scribbles
                    are allowed.
  onCreation:       function(elems, scope)
                    Event handler called when an instance of this
                    scribble is created.  The scope param is the name
                    of the scribble that this scribble is nested inside,
                    or 'global' if it is within the global scope. The
                    elems param is an object that contains all of the
                    primary jQuery DOM elements created to represent
                    this scribble.  The structure of this object is
                    as follows:
                    {
                      $name:    Element that represents the name of
                            the scribble.
                      $param:   An object that contains all of the
                            parameter field elements.  The keys
                            are the parameter names and the
                            element is the value of that key.
                      $display: An array of elements that represent
                            each line of text created from the
                            display string.
                      $scopes:  An array that contains all of the
                            scope elements created for this
                            scribble.
                    }
  onRemove:         function(params, scope)
                    Event handler called when an instance of this
                    scribble is about to be removed.  The scope param
                    is the name of the scribble that this scribble is
                    ested inside, or 'global' if it is within the
                    global scope.
  onEdit:           function(oldParams, newParams)
                    Event handler called whenever any parameter
                    value has been changed.  The old and new
                    params passed in are in the form of an object
                    that contains name:value pairs.
  script:           function(params, scopes)
                    function that returns a coded string to generate
                    raw Javascript code from.  The passed in params
                    is the same params object above except it contains
                    all of the current parameter values to be used.
                    The passed in scopes is an array of all scopes,
                    each array index coresponds to each scope ID
                    valid for this scribble.  Note that you will need
                    to include the scope brackets if necessary.
}
*/

$(document).ready(function() {
  // {}
  window.SCRBBLER.scribbleList.push({
    name: '{',
    description: '',
    params: {},
    display: '{{SCOPE}}}',
    onCreation: function(elems, scope) {
      elems.$name.removeClass('SCRBBLER-scribble-syntax');
    },
    script: function(params, scopes) {
      return '{' + scopes[0] + '}';
    },
  });

  // comment
  window.SCRBBLER.scribbleList.push({
    name: '//',
    description: '',
    params: {
              comment: '',
            },
    display: '  {{comment}}',
    onCreation: function(elems, scope) {
      elems.$name.addClass('SCRBBLER-scribble-comment');
      elems.$param.comment.addClass('SCRBBLER-scribble-comment');
      for (var index = 0; index < elems.$display.length; ++index) {
        elems.$display[index].addClass('SCRBBLER-scribble-comment');
      }
    },
    script: function(params, scopes) {
      return '// ' + params.comment;
    },
  });

  // comment
  window.SCRBBLER.scribbleList.push({
    name: '/*',
    description: '',
    params: {},
    display: '{{SCOPE}}*/',
    onCreation: function(elems, scope) {
      elems.$name.addClass('SCRBBLER-scribble-comment');
      for (var index = 0; index < elems.$display.length; ++index) {
        elems.$display[index].addClass('SCRBBLER-scribble-comment');
      }
      for (var index = 0; index < elems.$scopes.length; ++index) {
        elems.$scopes[index].addClass('SCRBBLER-scribble-comment');
      }
    },
    script: function(params, scopes) {
      return '/*' + scopes[0] + '*/';
    },
  });

  // var
  window.SCRBBLER.scribbleList.push({
    name: 'var',
    description: '',
    params: {
              name: 'name',
              value: '',
            },
    display: '  {{name}} = {{value}};',
    script: function(params, scopes) {
      if (params.value !== '') {
        return 'var ' + params.name + ' = ' + params.value + ';';
      } else {
        return 'var ' + params.name + ';';
      }
    },
  });

  // if
  window.SCRBBLER.scribbleList.push({
    name: 'if',
    description: '',
    params: {
              params: 'true',
            },
    display: '  ({{params}}) {{{SCOPE}}}',
    script: function(params, scopes) {
      return 'if (' + params.params + ') {' + scopes[0] + '}';
    },
  });

  // else if
  window.SCRBBLER.scribbleList.push({
    name: 'else if',
    description: '',
    params: {
              params: 'true',
            },
    display: '  ({{params}}) {{{SCOPE}}}',
    script: function(params, scopes) {
      return 'else if (' + params.params + ') {' + scopes[0] + '}';
    },
  });

  // else
  window.SCRBBLER.scribbleList.push({
    name: 'else',
    description: '',
    params: {},
    display: '  {{{SCOPE}}}',
    script: function(params, scopes) {
      return 'else {' + scopes[0] + '}';
    },
  });

  // do
  window.SCRBBLER.scribbleList.push({
    name: 'do',
    description: '',
    params: {
              condition: 'true',
            },
    display: '  {{{SCOPE}}} <span class="SCRBBLER-scribble-syntax">while</span> ({{condition}});',
    script: function(params, scopes) {
      return 'do\n{' + scopes[0] + '} while (' + params.condition + ');';
    },
  });

  // while
  window.SCRBBLER.scribbleList.push({
    name: 'while',
    description: '',
    params: {
              condition: 'true'
            },
    display: '  ({{condition}}) {{{SCOPE}}}',
    script: function(params, scopes) {
      return 'while (' + params.condition + ') {' + scopes[0] + '}';
    },
  });

  // for
  window.SCRBBLER.scribbleList.push({
    name: 'for',
    description: '',
    params: {
              index: 'index',
              count: 'count',
              reverse: 'false',
            },
    display: '  (var {{index}}; count = {{count}}; reverse = {{reverse}}) {{{SCOPE}}}',
    script: function(params, scopes) {
      if (params.reverse === 'false' || params.reverse === '' || params.reverse === '0') {
        return 'for (var ' + params.index + ' = 0;' + params.index + ' < ' + params.count + '; ++' + params.index + ') {' + scopes[0] + '}';
      } else {
        return 'for (var ' + params.index + ' = ' + params.count + ' - 1; ' + params.index + ' >= 0; --' + params.index + ') {' + scopes[0] + '}';
      }
    },
  });

  // for each
  window.SCRBBLER.scribbleList.push({
    name: 'for each',
    description: '',
    listed: true,
    params: {
              value: 'value',
              array: 'array',
            },
    display: '  (var {{value}} in {{array}}) {{{SCOPE}}}',
    script: function(params, scopes) {
      return 'for (var i = 0, ' + params.value + '; i < ' + params.array + '.length, ' + params.value + ' = ' + params.array + '[i]; ++i) {' + scopes[0] + '}';
    },
  });

  // switch
  window.SCRBBLER.scribbleList.push({
    name: 'switch',
    description: '',
    params: {
              condition: 'value',
            },
    display: '  ({{condition}}) {{{SCOPE}}}',
    canScopeContain: function(scribble) {
      if (scribble === 'case' || scribble === 'default') {
        return true;
      }
      return false;
    },
    script: function(params, scopes) {
      return 'switch (' + params.condition + ') {' + scopes[0] + '}';
    },
  });

  // case
  window.SCRBBLER.scribbleList.push({
    name: 'case',
    description: '',
    params: {
              case: '0',
            },
    display: '  {{case}}: {{{SCOPE}}}',
    canBeInScope: function(scope, scopeID) {
      if (scope === 'switch') {
        return true;
      }
      return false;
    },
    script: function(params, scopes) {
      return 'case ' + params.case + ': {' + scopes[0] + '}';
    },
  });

  // default
  window.SCRBBLER.scribbleList.push({
    name: 'default',
    description: '',
    params: {},
    display: ': {{{SCOPE}}}',
    canBeInScope: function(scope, scopeID) {
      if (scope === 'switch') {
        return true;
      }
      return false;
    },
    script: function(params, scopes) {
      return 'default: {' + scopes[0] + '}';
    },
  });

  // break
  window.SCRBBLER.scribbleList.push({
    name: 'break',
    description: '',
    params: {},
    display: ';',
    canBeInScope: function(scope, scopeID) {
      if (scope !== 'global') {
        return true;
      }
      return false;
    },
    script: function(params, scopes) {
      return 'break;';
    },
  });

  // return
  window.SCRBBLER.scribbleList.push({
    name: 'return',
    description: '',
    params: {
              value: '',
            },
    display: '  {{value}};',
    canBeInScope: function(scope, scopeID) {
      if (scope !== 'global') {
        return true;
      }
      return false;
    },
    script: function(params, scopes) {
      if (params.value) {
        return 'return ' + params.value + ';';
      }
      return 'return;';
    },
  });

  // function
  window.SCRBBLER.scribbleList.push({
    name: 'function',
    description: '',
    params: {
              name: '',
              params: '',
            },
    display: '  {{name}}({{params}}) {{{SCOPE}}}',
    script: function(params, scopes) {
      if (params.name) {
        return 'function ' + params.name + '(' + params.params + ') {' + scopes[0] + '}';
      }
      return 'function(' + params.params + ') {' + scopes[0] + '}';
    },
  });

  // (function)()
  window.SCRBBLER.scribbleList.push({
    name: '(function',
    description: '',
    params: {
              params: '',
              input: '',
            },
    display: '({{params}}) {{{SCOPE}}})({{input}});',
    script: function(params, scopes) {
      return '(function(' + params.params + ') {' + scopes[0] + '})(' + params.input + ');';
    },
  });

  // new
  window.SCRBBLER.scribbleList.push({
    name: 'new',
    description: '',
    params: {
              value: 'value',
            },
    display: '  {{value}};',
    script: function(params, scopes) {
      return 'new ' + params.value + ';';
    },
  });

  // delete
  window.SCRBBLER.scribbleList.push({
    name: 'delete',
    description: '',
    params: {
              value: 'value',
            },
    display: '  {{value}};',
    script: function(params, scopes) {
      return 'delete ' + params.value + ';';
    },
  });

  // try catch
  window.SCRBBLER.scribbleList.push({
    name: 'try',
    description: '',
    params: {
              catchVar: 'e',
            },
    display: '  {{{SCOPE}}} <span class="SCRBBLER-scribble-syntax">catch</span>({{catchVar}}) {{{SCOPE}}}',
    script: function(params, scopes) {
      return 'try {' + scopes[0] + '} catch(' + params.catchVar + ') {' + scopes[1] + '}';
    },
  });

  // throw
  window.SCRBBLER.scribbleList.push({
    name: 'throw',
    description: '',
    params: {
              error: 'error',
            },
    display: '  {{error}};',
    script: function(params, scopes) {
      return 'throw ' + params.error;
    },
  });

  // onload
  window.SCRBBLER.scribbleList.push({
    name: 'onload',
    description: '',
    listed: true,
    params: {},
    display: '  function() {{{SCOPE}}}',
    script: function(params, scopes) {
      return '$(document).ready(function() {' + scopes[0] + '});';
    },
  });

  // new scribble
  window.SCRBBLER.scribbleList.push({
    name: 'scribble',
    description: '',
    listed: true,
    params: {
              name: '',
              description: '',
              params: '"variable":"default"',
              display: '',
              canBeInScope: '',
              canScopeContain: '',
              onCreation: '',
              onRemove: '',
              onEdit: '',
              script: '',
            },
    display: '  {{name}}({\n\tdescription: {{description}}\n\tparams: {{{params}}},\n\tdisplay: {{display}},\n\tcanBeInScope: function(scribble, scopeID) {\n\t\tvar result = true;\n\t\t{{canBeInScope}}\n\t\treturn result;\n\t},\n\tcanScopeContain: function(scribble) {\n\t\tvar result = true;\n\t\t{{canScopeContain}}\n\t\treturn result;\n\t},\n\tonCreation: function(elems, scope) {\n\t\t{{onCreation}}\n\t},\n\tonRemove: function(params, scope) {\n\t\t{{onRemove}}\n\t},\n\tonEdit: function(oldParams, newParams) {\n\t\t{{onEdit}}\n\t},\n\tscript: function(params, scopes) {\n\t\tvar result;\n\t\t{{script}}\n\t\treturn result;\n\t}\n});',
    onEdit: function(oldParams, newParams) {
      // First search if we already have a scribble made.
      var found = -1;
      if (oldParams.name) {
        for (var i = window.SCRBBLER.scribbleList.length-1; i >= 0; --i) {
          if (window.SCRBBLER.scribbleList[i].dynamic &&
            window.SCRBBLER.scribbleList[i].name === oldParams.name) {
            found = i;
            break;
          }
        }
      }

      if (found > -1) {
        // If we found an existing scribble, modify it to match any changes.
        if (newParams.name) {
          window.SCRBBLER.scribbleList[found].name = newParams.name;
        } else {
          // The name is not valid, remove the scribble.
          window.SCRBBLER.scribbleList.splice(found, 1);
          return;
        }

        var scribble = window.SCRBBLER.scribbleList[found];
        scribble.name = newParams.name;
        scribble.description = newParams.description;
        scribble.params = JSON.parse('{'+newParams.params+'}');
        scribble.display = newParams.display;
        scribble.canBeInScope = new Function('scribble, scopeID', 'var result = true;'+newParams.canBeInScope+';return result;');
        scribble.canScopeContain = new Function('scribble', 'var result = true;'+newParams.canScopeContain+';return result;');
        scribble.onCreation = new Function('elems, scope', newParams.onCreation);
        scribble.onRemove = new Function('params, scope', newParams.onRemove);
        scribble.onEdit = new Function('oldParams, newParams', newParams.onEdit);
        scribble.script = new Function('params, scopes', 'var result;'+newParams.script+';return result;');
      } else if (newParams.name) {
        // No scribble exists yet, so create a new one.
        window.SCRBBLER.scribbleList.push({
          name: newParams.name,
          description: newParams.description,
          listed: true,
          dynamic: true,
          params: JSON.parse('{'+newParams.params+'}'),
          display: newParams.display,
          canBeInScope: new Function('scribble, scopeID', 'var result = true;'+newParams.canBeInScope+';return result;'),
          canScopeContain: new Function('scribble', 'var result = true;'+newParams.canScopeContain+';return result;'),
          onCreation: new Function('elems, scope', newParams.onCreation),
          onRemove: new Function('params, scope', newParams.onRemove),
          onEdit: new Function('oldParams, newParams', newParams.onEdit),
          script: new Function('params, scopes', 'var result;'+newParams.script+';return result;'),
        });
      }
    },
    script: function(params) {
      return '';
    },
  });});
  