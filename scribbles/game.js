///////////////////////////////////////////////////////////
// Core Scribbles
// These are the core implementation of scribbles, basically
// containing basic javascript scribbles.
///////////////////////////////////////////////////////////

/* Scribble Structure:
{
  name:             The name of the scribble, must be unique.
  description:      optional description about the scribble.
  listed:           An optional boolean value that determines
                    whether this scribble shows up in the
                    drop down suggestion box.
  params:           The expected parameters in the scribble,
                    defined as an object with name:value pairs.
  display:          A coded html string that defines how the scribble
                    block is displayed in the UI.
                    - A double space at the very start of this string
                      will be replaced with an html '&nbsp', this
                      allows there to be a space between the scribble
                      name displayed and the display defined here.
                    - Use {{SCOPE}} to include a scope where other
                      scribbles can be nested.  Any number of
                      scopes can be included and will later bee
                      distinguished via index number in an array.
                    - Wrap any parameter name in double curley
                      braces '{{}}' to have that param substituted
                      with an editable field. 
  canBeInScope:     function(scribble, scopeID) return boolean
                    An optional function that is called to validate
                    whether this scribble can be part of another
                    scribble's scope.  By default, all scopes
                    are allowed.
  canScopeContain:  function(scribble) return boolean
                    An optional function that is called to validate
                    whether a given scribble can be nested within
                    the requested scope.  By default, all scribbles
                    are allowed.
  onCreation:       function(elems, scope)
                    Event handler called when an instance of this
                    scribble is created.  The scope param is the name
                    of the scribble that this scribble is nested inside,
                    or 'global' if it is within the global scope. The
                    elems param is an object that contains all of the
                    primary jQuery DOM elements created to represent
                    this scribble.  The structure of this object is
                    as follows:
                    {
                      $name:    Element that represents the name of
                            the scribble.
                      $param:   An object that contains all of the
                            parameter field elements.  The keys
                            are the parameter names and the
                            element is the value of that key.
                      $display: An array of elements that represent
                            each line of text created from the
                            display string.
                      $scopes:  An array that contains all of the
                            scope elements created for this
                            scribble.
                    }
  onRemove:         function(params, scope)
                    Event handler called when an instance of this
                    scribble is about to be removed.  The scope param
                    is the name of the scribble that this scribble is
                    ested inside, or 'global' if it is within the
                    global scope.
  onEdit:           function(oldParams, newParams)
                    Event handler called whenever any parameter
                    value has been changed.  The old and new
                    params passed in are in the form of an object
                    that contains name:value pairs.
  script:           function(params, scopes)
                    function that returns a coded string to generate
                    raw Javascript code from.  The passed in params
                    is the same params object above except it contains
                    all of the current parameter values to be used.
                    The passed in scopes is an array of all scopes,
                    each array index coresponds to each scope ID
                    valid for this scribble.  Note that you will need
                    to include the scope brackets if necessary.
}
*/

$(document).ready(function() {
  // spawn npc
  window.SCRBBLER.scribbleList.push({
    name: 'spawn npc',
    description: '',
    params: {
              type: 'type name',
              name: 'name',
              loc: 'location',
            },
    listed: true,
    display: '  of type {{type}} with name {{name}} at {{loc}};',
    script: function(params, scopes) {
      return 'spawnNPC(' + params.npc + ', ' + params.loc + ');';
    },
  });

  // spawn tile
  window.SCRBBLER.scribbleList.push({
    name: 'spawn tile',
    description: '',
    params: {
              tile: 'tile type',
              loc: 'location',
            },
    listed: true,
    display: '  of type {{tile}} at {{loc}};',
    script: function(params, scopes) {
      return 'spawnTile(' + params.tile + ', ' + params.loc + ');';
    },
  });

  // teleport
  window.SCRBBLER.scribbleList.push({
    name: 'teleport',
    description: '',
    params: {
              npc: 'name',
              loc: 'location',
            },
    listed: true,
    display: '  npc named {{npc}} to {{loc}};',
    script: function(params, scopes) {
      return 'teleportNPC(' + params.npc + ', ' + params.loc + ');';
    },
  });

  // get location
  window.SCRBBLER.scribbleList.push({
    name: 'get location',
    description: '',
    params: {
              value: 'loc',
              x: '0',
              y: '0',
            },
    listed: true,
    display: '  as var {{value}} from (x: {{x}}, y: {{y}})',
    script: function(params, scopes) {
      return 'var ' + params.value + ' = {x:' + params.x + ', y:' + params.y + '};';
    }
  });
});
