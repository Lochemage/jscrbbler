(function(init) {
  // Initialize our SCRBBLER object and assign it to the global scope.
  window.SCRBBLER = init();

})(function() {
  // This is our init function, it will initialize our
  // SCRBBLER object and return it.

  // First, lets define a few constants for use later.
  var EDITOR_TEMPLATE = "<ol class='SCRBBLER-editor'></ol>";
  var SCRIBBLE_ADDER_TEMPLATE = "<li class='mjs-nestedSortable-no-nesting'><div><div class='SCRBBLER-scribble-insert'/><span class='SCRBBLER-scribble-name'></span></div></li>";
  var SCRIBBLE_NAME_TEMPLATE = "<span class='SCRBBLER-scribble-name'></span>"
  var FIELD_TEMPLATE = "<div></div>";
  var FIELD_INPUT_TEMPLATE = "<input type='text' class='SCRBBLER-scribble-edit'>";
  var SCRIBBLE_INPUT_TEMPLATE = "<input type='text' class='SCRBBLER-scribble-edit' list='SCRBBLER-scribbleList'>";
  var SCRIBBLE_LIST_TEMPLATE = "<datalist id='SCRBBLER-scribbleList'></datalist>";
  var SCRIBBLE_ITEM_TEMPLATE = "<option></option>";
  var SCRIBBLE_INFO_TEMPLATE = "<div class='SCRBBLER-scribble-info'></div>";
  var SCRIBBLE_SCOPE_TEMPLATE = "<ol class='SCRBBLER-scribble-scope'></ol>";
  var SCRIBBLE_POST_TEMPLATE = "<span class='SCRBBLER-scribble-post'></span>";
  var LINE_BREAK_TEMPLATE = "<br/>";

  var PLACEHOLDER_ADD_SCRIBBLE_TEXT = "";


  var _editorId   = 0;
  var _editorList = [];
  var _lastError  = '';
  var _indent     = '  ';

  var _activeEdit = null;

  /**
   * Handles the creation of the editor object.
   * Returns false on failure.
   */
  function _createEditor(elem) {
    if (_findEditor(elem).index > -1) {
      _lastError = "A SCRBBLER editor already exists using this element.";
      return false;
    }

    // Create our SCRBBLER editor root node.
    var $root = $(EDITOR_TEMPLATE);
    $root.attr('id', 'SCRBBLER-' + _editorId);
    _editorId++;

    var $elem = $(elem);
    $elem.append($root);

    var $scribbleAdder = $(SCRIBBLE_ADDER_TEMPLATE);
    $root.append($scribbleAdder);
    _createScribbleAdder($scribbleAdder);

    ///////////////////////////////////////////////////////////
    /* Public SCRBBLER Editor API                             */
    ///////////////////////////////////////////////////////////
    var editor = {
      /**
       * Detach this SCRBBLER editor from the document.
       */
      detach: function() {
        var found = _findEditor(elem);
        if (found.index > -1) {
          $elem.find('.SCRBBLER-editor').remove();
          _editorList.splice(found.index, 1);
          return true;
        }

        _lastError = 'Detach failed, editor not properly listed';
        return false;
      },

      /*
       * Encode the contents of this SCRBBLER editor to an encoded string
       * that can later be used to restore the editor status.
       *
       * Also see decode()
       */
      encode: function() {
        // TODO:
        _lastError = 'Encode failed, not implemented yet';
        return false;
      },

      /**
       * Decodes a previously encoded string and populates the SCRBBLER
       * editor to match.
       *
       * Also see encode()
       */
       decode: function(code) {
        // TODO:
        _lastError = 'Decode failed, not implemented yet';
        return false;
      },

      /**
       * Generate raw javascript code from this SCRBBLER
       */
      script: function() {
        // Create our recursive function that recurses through nested scopes.
        function _scope($scope, nestCount) {
          var indent = '';
          for (var i = 0; i < nestCount; ++i) {
            indent += _indent;
          }
          var result = '';
          var $scribbles = $scope.children('.SCRBBLER-scribble, .SCRBBLER-scribble-adder');
          $scribbles.each(function() {
            var $div = $(this).children('div');

            // Find the scribble being used.  Incase the name is
            // currently being edited, also search the edit widget
            var name = $div.children('.SCRBBLER-scribble-name').text();
            if (!name) {
              name = $div.children('input').val();
            }

            // Once we know the name of the scribble, retrieve that
            // scribbles data object.
            var scribble = _findScribble(name);

            var params = _getElementParameters($div, scribble);

            var script = '';
            if (scribble) {
              var subScopes = ['SCOPE', 'SCOPE', 'SCOPE', 'SCOPE', 'SCOPE', 'SCOPE', 'SCOPE', 'SCOPE', 'SCOPE', 'SCOPE'];

              var $subScopes = $div.children('.SCRBBLER-scribble-scope');
              for (var subScopeIndex = 0; subScopeIndex < $subScopes.length; ++subScopeIndex) {
                subScopes[subScopeIndex] = '\n' + _scope($($subScopes[subScopeIndex]), nestCount + 1);
              }

              script = scribble.script(params, subScopes);
              if (typeof script === 'string') {
                script = script.replace(/\t/g, _indent);
                script = script.replace(/\n/g, '\n' + indent);
              }
            } else if (typeof name === 'string') {
              script = name;
            } else {
              return;
            }
            result += indent + script + '\n';
          });

          return result;
        };

        return _scope($root, 0);
      },
    };

    _editorList.push({
      elem:   elem,
      $root:  $root,
      editor: editor,
    });
    
    return editor;
  };

  /**
   * Populates the UI for the given scribble.
   */
  function _createScribble($elem, data, scope) {
    if (!data) {
      return;
    }

    var elemsData = {};
    elemsData.$name = null;
    elemsData.$param = {};
    elemsData.$display = [];
    elemsData.$scopes = [];

    var $name = $elem.children('.SCRBBLER-scribble-name').first();
    if ($name.length) {
      elemsData.$name = $name;
      $name.addClass('SCRBBLER-scribble-syntax');
    }

    var $info = $(SCRIBBLE_INFO_TEMPLATE);
    $name.after($info);

    var display = data.display;
    if (display.indexOf('  ') === 0) {
      display = display.replace('  ', '&nbsp');
    }

    for (var paramName in data.params) {
      if (data.params.hasOwnProperty(paramName)) {
        display = display.replace('{{' + paramName + '}}', '<div id=' + paramName + '>' + data.params[paramName] + '</div>');
      }
    }

    // Replace all tab codes with html spaces.
    display = display.replace(/\t/g, '&nbsp&nbsp&nbsp&nbsp&nbsp');

    // Find all scopes within the display string and split it into multiple
    // display strings that contain a scope in between.
    var $next = $info;
    var scopePos = display.indexOf('{{SCOPE}}');
    var linePos = display.indexOf('\n');
    var lastLine = false;
    while (scopePos !== -1 || linePos !== -1 || !lastLine) {
      var lineText = '';
      var hasScope = false;
      if (!lastLine && scopePos === -1 && linePos === -1) {
        // Nothing to split, display all.
        lineText = display;
        display = '';
        lastLine = true;
      } else if (scopePos === -1 || (linePos !== -1 && scopePos > linePos)) {
        // Add a new display line.
        lineText = display.substring(0, linePos);
        display = display.substring(linePos + 1);
      } else if (scopePos !== -1) {
        // Add a new scope.
        lineText = display.substring(0, scopePos);
        display = display.substring(scopePos + 9);
        hasScope = true;
      }

      // Special case for the very first line, the element is already made
      // so we just need to use it.
      if (elemsData.$display.length === 0) {
        $info.html(lineText);
        elemsData.$display.push($info);
      } else {
        var $post = $(SCRIBBLE_POST_TEMPLATE);
        $post.html(lineText);
        elemsData.$display.push($post);
        $next.after($post);
        $next = $post;
      }

      if (hasScope) {
        var $scope = $(SCRIBBLE_SCOPE_TEMPLATE);
        $scope.attr('id', elemsData.$scopes.length);
        $next.after($scope);
        $next = $scope;
        elemsData.$scopes.push($scope);

        var $scribbleAdder = $(SCRIBBLE_ADDER_TEMPLATE);
        $scope.append($scribbleAdder);
        _createScribbleAdder($scribbleAdder);
      }

      scopePos = display.indexOf('{{SCOPE}}');
      linePos = display.indexOf('\n');
    }

    // Now find each parameter field and make them into an actual editable widget
    for (var paramName in data.params) {
      if (data.params.hasOwnProperty(paramName)) {
        $elem.find('#' + paramName).each(function() {
          _createEditField($(this), $elem);
          elemsData.$param[paramName] = $(this);
        });
      }
    }

    if (data.onCreation) {
      data.onCreation(elemsData, scope);
    }
  };

  /**
   * Creates a clickable edit that allows you to add a new scribble.
   */
  function _createScribbleAdder($elem) {
    if ($elem.hasClass('SCRBBLER-field') ||
      $elem.hasClass('SCRBBLER-scribble-adder') ||
      $elem.hasClass('SCRBBLER-scribble')) {
      return false;
    }

    $elem.addClass('SCRBBLER-scribble-adder');
    var $elemDiv = $elem.children('div').first();

    _enableElementDragDrop($elem.parent());

    // Add and populate our drop list of available scribbles for use.
    var $name = $elem.find('.SCRBBLER-scribble-name');
    $name.text(PLACEHOLDER_ADD_SCRIBBLE_TEXT);
    var oldValue = "";
    var value = "";

    var $insert = $elem.find('.SCRBBLER-scribble-insert');
    $insert.click(function(event) {
      if ($(this).parent().is('div') && $(this).parent().parent().hasClass('SCRBBLER-scribble-adder')) {
        return;
      }

      event.stopPropagation();

      var $scribbleAdder = $(SCRIBBLE_ADDER_TEMPLATE);
      $scribbleAdder.insertBefore($elem);
      _createScribbleAdder($scribbleAdder);
      $scribbleAdder.click();
    });

    $elem.click(function(event) {
      event.stopPropagation();

      if (_activeEdit) {
        _activeEdit.blur();
      }

      // Replace the scribble's name with an input field.
      $name.hide();
      var $edit = $(SCRIBBLE_INPUT_TEMPLATE);
      var $insert = $elemDiv.children('.SCRBBLER-scribble-insert').first();
      $edit.insertAfter($insert);
      $edit[0].value = value;
      $edit.focus();
      $edit.select();
      _activeEdit = $edit;

      // Find our active scope.
      var scope = 'global';
      var id = 0;
      var $parents = $elem.parents('li');
      if ($parents.length) {
        scope = $parents.children('div').children('.SCRBBLER-scribble-name').text();
      }
      var $parents = $elem.parents('ol');
      if ($parents.length) {
        id = $parents.attr('id');
      }

      // Update our list of available scribbles that can be part of this scope.
      _updateScribbleList($elemDiv, scope, id);

      // Workaround for Chrome losing selection.      
        $edit.mouseup(function() {
            $edit.unbind("mouseup");
            return false;
        });
      
      function apply(autoFocus) {
        _activeEdit = null;
        value = $edit[0].value;

        // Remove the edit field and restore basic name text.
        $edit.remove();
        $name.show();
        // $elemDiv.children('.SCRBBLER-scribble-insert').first().after($name);

        var $parent = $elem.parent();

        // If we are changing the scribble, reload it.
        if (value != oldValue) {
          var scribble = _findScribble(oldValue);
          if (scribble && scribble.onRemove) {
            var params = _getElementParameters($elemDiv, scribble);
            scribble.onRemove(params, scope);
          }

          oldValue = value;

          $elem.removeClass('SCRBBLER-scribble').removeClass('SCRBBLER-scribble-adder');

          var displayValue = value;
          if (displayValue === "") {
            displayValue = PLACEHOLDER_ADD_SCRIBBLE_TEXT;
            $elem.addClass('SCRBBLER-scribble-adder');
          } else {
            $elem.addClass('SCRBBLER-scribble');
          }

          $name.html(displayValue);

          // Remove any existing scribble info.
          var $info = $elemDiv.children('.SCRBBLER-scribble-info, .SCRBBLER-scribble-scope, .SCRBBLER-scribble-post');
          $info.remove();

          // If this is no longer empty, make sure we add a new empty one.
          if (value !== "") {
            // Populate the scribble info.
            _createScribble($elemDiv, _findScribble(value), scope);

            // Make sure we create a new adder if the current line used it.
            var $adderList = $elem.parent().children('.SCRBBLER-scribble-adder');
            var $lastAdder = $adderList.last();
            if (!$lastAdder.length || $lastAdder.find('.SCRBBLER-scribble-name').text() !== PLACEHOLDER_ADD_SCRIBBLE_TEXT) {
              var $scribbleAdder = $(SCRIBBLE_ADDER_TEMPLATE);
              $elem.parent().append($scribbleAdder);
              _createScribbleAdder($scribbleAdder);
            }
          }

          _updateFocus($elem, autoFocus);
        } else {
          var displayValue = oldValue;
          if (displayValue === "") {
            displayValue = PLACEHOLDER_ADD_SCRIBBLE_TEXT;
          }
          $name.html(displayValue);
          // if (_findScribble(value)) {
          //   $name.addClass('SCRBBLER-scribble-syntax');
          // }

          _updateFocus($elem, autoFocus);
        }
      }

      // Apply any edit changes to the element.
      $edit.change(function(){apply('down');});
      $edit.blur(function(){apply('none');});

      $edit.keydown(function(event) {
        // Escape key to cancel edit.
        if (event.keyCode == 27) {
          var val = oldValue;
          if (oldValue === "") {
            val = PLACEHOLDER_ADD_SCRIBBLE_TEXT;
          }
          $edit.remove();
          $name = $(SCRIBBLE_NAME_TEMPLATE);
          $elem.find('.SCRBBLER-scribble-insert').after($name);
          $name.html(val);
          _updateFocus($elem, 'none');
          event.preventDefault();
        // Shift + Tab to cycle backwards.
        } else if (event.keyCode == 9 && event.shiftKey) {
          apply('left');
          event.preventDefault();
        // Tab key to cycle next.
        } else if (event.keyCode == 9) {
          apply('right');
          event.preventDefault();
        // Return to apply changes and cycle forward.
        } else if (event.keyCode == 13) {
          apply('return');
          event.preventDefault();
        // Down arrow to cycle down.
        } else if (event.keyCode == 40) {
          apply('down');
          event.preventDefault();
        // Up arrow to cycle up.
        } else if (event.keyCode == 38) {
          apply('up');
          event.preventDefault();
        }
      });
    });
  };

  /**
   * Creates a clickable edit field and appends it to the given element.
   */
  function _createEditField($elem, $parent) {
    if ($elem.hasClass('SCRBBLER-field') || $elem.hasClass('SCRBBLER-scribble-adder')) {
      return false;
    }

    var value = $elem.html();
    $elem.addClass('SCRBBLER-field');

    $elem.click(function(event) {
      event.stopPropagation();

      if (_activeEdit) {
        _activeEdit.blur();
      }

      $elem.html(FIELD_INPUT_TEMPLATE);

      $edit = $elem.find('input');
      $edit[0].value = value;
      $edit.focus();
      $edit.select();
      _activeEdit = $edit;
      
      // Workaround for Chrome losing selection.      
        $edit.mouseup(function() {
            $edit.unbind("mouseup");
            return false;
        });

      function apply(autoFocus) {
        _activeEdit = null;
        var newValue = $edit[0].value;

        var name = $parent.children('.SCRBBLER-scribble-name').text();
        var scribble = _findScribble(name);
        if (scribble) {
          if (scribble.validate) {
            newValue = scribble.validate($elem.attr('id'), value, newValue);
            $edit[0].value = newValue;
          }
        }
  
        var oldParams = _getElementParameters($parent, scribble);
        oldParams[$elem.attr('id')] = value;
        value = newValue;
        $elem.html(value);

        if (scribble && scribble.onEdit) {
          var newParams = _getElementParameters($parent, scribble);
          scribble.onEdit(oldParams, newParams);
        }
        _updateFocus($elem, autoFocus);
      }

      // Apply any edit changes to the element.
      $edit.change(function(){apply('down');});
      $edit.blur(function(){apply('none');});

      $edit.keydown(function(event) {
        // Escape to cancel edit.
        if (event.keyCode == 27) {
          $elem.html(value);
          event.preventDefault();
        // Shift + Tab to cycle backwards.
        } else if (event.keyCode == 9 && event.shiftKey) {
          apply('left');
          event.preventDefault();
        // Return to apply changes and cycle forward.
        } else if (event.keyCode == 13) {
          apply('return');
          event.preventDefault();
        // Tab key to cycle next.
        } else if (event.keyCode == 9) {
          apply('right');
          event.preventDefault();
        // Down arrow to cycle down.
        } else if (event.keyCode == 40) {
          apply('down');
          event.preventDefault();
        // Up arrow to cycle up.
        } else if (event.keyCode == 38) {
          apply('up');
          event.preventDefault();
        }
      });
    });
  };

  /**
   * Updates our internal datalist of scribbles.
   * This function is automatically called whenever
   * a new editor is created.  You only need to use
   * this function if you are dynamically creating
   * new scribbles.
   */
  function _updateScribbleList($elem, scope, scopeID) {
    var $scribbleList = $('body').find('#SCRBBLER-scribbleList');
    // We only need one datalist element in our document
    if ($scribbleList.length === 0) {
      $scribbleList = $(SCRIBBLE_LIST_TEMPLATE);
      $('body').append($scribbleList);
    }

    // First, if we are in a scope, find the scribble that matches this scope.
    // We will later use it's canBeInScope and 
    var scopeData = _findScribble(scope);

    $scribbleList.children().remove();
    for (var i = 0; i < window.SCRBBLER.scribbleList.length; ++i) {
      var data = window.SCRBBLER.scribbleList[i];
      if (!scopeData || !scopeData.canScopeContain || scopeData.canScopeContain(data.name)) {
        if (window.SCRBBLER.scribbleList[i].listed && 
          (!window.SCRBBLER.scribbleList[i].canBeInScope ||
          window.SCRBBLER.scribbleList[i].canBeInScope(scope, scopeID))) {
          var $option = $(SCRIBBLE_ITEM_TEMPLATE);
          $option[0].value = window.SCRBBLER.scribbleList[i].name;
          if (window.SCRBBLER.scribbleList[i].description) {
            $option.html(" (" + window.SCRBBLER.scribbleList[i].description + ")");
          }
          $scribbleList.append($option);
        }
      }
    }

    // TODO: Go through every active editor and repair any
    // that are still using a scribble that is gone or changed.
  };

  /**
   * Find an editor by its element.
   */
  function _findEditor(elem) {
    for (var i = 0; i < _editorList.length; ++i) {
      if (elem === _editorList[i].elem) {
        return {
          index:i,
          editor:_editorList[i].editor
        };
      }
    }

    return {
      index:-1,
      editor:null,
    }
  };

  /**
   * Finds a scribble of a matching name.
   */
  function _findScribble(name) {
    for (var i = 0; i < window.SCRBBLER.scribbleList.length; ++i) {
      if (window.SCRBBLER.scribbleList[i].name === name) {
        return window.SCRBBLER.scribbleList[i];
      }
    }

    return null;
  }

  /**
   * Find all parameters to a given scribble
   */
  function _getElementParameters($elemDiv, data) {
    var params = {};
    if (data && data.params) {
      for (var paramName in data.params) {
        if (data.params.hasOwnProperty(paramName)) {
          params[paramName] = 'PARAM';
        }
      }
    }
    // Find all user input field params.
    var $fields = $elemDiv.children('.SCRBBLER-scribble-info, .SCRBBLER-scribble-post').children('.SCRBBLER-field');
    $fields.each(function() {
      var paramName = $(this).attr('id');
      var paramValue = $(this).text();

      // Just incase the parameter is currently in edit mode,
      // pull the current value from that editor.
      var $input = $(this).children('input');
      if ($input.length) {
        paramValue = $input.val();
      }
      params[paramName] = paramValue;
    });
    return params;
  }

  function _enableElementDragDrop($elem) {
    // $elem.nestedSortable('destroy');
    $elem.nestedSortable({
      items: '.SCRBBLER-scribble',
      forcePlaceholderSize: true,
      connectWith: '.SCRBBLER-editor, .SCRBBLER-scribble-scope',
      handle: 'div',
      toleranceElement: '> div',
    });

    var updating = false;
    $elem.parent().on("sortupdate", function(event, ui) {
      // var $list = ui.item.parent().find('.SCRBBLER-scribble, .SCRBBLER-scribble-adder');
      // if ($list.index(ui.item) === $list.length - 1) {

      // }
      if (!updating) {
        updating = true;
        _cleanupFocus(ui.item);
        setTimeout(function() {
          updating = false;
        }, 500);
      }
    });
  };

  /**
   * Find the next edit to focus on.
   * an optional option can be supplied to define any
   * specific method of selection beyond the normal.
   */
  function _updateFocus($elem, option) {
    if (option !== 'none') {
      if (option === 'return') {
        _focusNext($elem, ['SCRBBLER-field', 'SCRBBLER-scribble-adder', 'SCRBBLER-scribble']);
      } else if (option === 'left') {
        _focusPrev($elem, ['SCRBBLER-field', 'SCRBBLER-scribble-adder', 'SCRBBLER-scribble']);
      } else if (option === 'right') {
        _focusNext($elem, ['SCRBBLER-field', 'SCRBBLER-scribble-adder', 'SCRBBLER-scribble']);
      } else if (option === 'up') {
        _focusPrev($elem, ['SCRBBLER-scribble', 'SCRBBLER-scribble-adder']);
      } else if (option === 'down') {
        _focusNext($elem, ['SCRBBLER-scribble', 'SCRBBLER-scribble-adder']);
      }
    }
    _cleanupFocus($elem);
  }

  function _focusPrev($elem, classes, $parent) {
    if (!$parent) {
      $parent = $elem.parents('.SCRBBLER-editor');
    }
    if ($parent.length) {
      var $scribbles = $parent.find('.SCRBBLER-scribble, .SCRBBLER-scribble-adder, .SCRBBLER-field');
      if ($scribbles.length) {
        var index = $scribbles.index($elem);
        while (index > 0) {
          index--;
          var $scribble = $($scribbles[index]);
          for (var i = 0; i < classes.length; ++i) {
            if ($scribble.hasClass(classes[i])) {
              $scribble.click();
              return true;
            }
          }
        }

        $elem.click();
        return false;
      }
    }
    return false;
  };

  function _focusNext($elem, classes, $parent) {
    if (!$parent) {
      $parent = $elem.parents('.SCRBBLER-editor');
    }
    if ($parent.length) {
      var $scribbles = $parent.find('.SCRBBLER-scribble, .SCRBBLER-scribble-adder, .SCRBBLER-field');
      if ($scribbles.length) {
        var index = $scribbles.index($elem);
        while (index < $scribbles.length - 1) {
          index++;
          var $scribble = $($scribbles[index]);
          for (var i = 0; i < classes.length; ++i) {
            if ($scribble.hasClass(classes[i])) {
              $scribble.click();
              return true;
            }
          }
        }

        $elem.click();
        return false;
      }
    }
    return false;
  };

  /**
   * Clean up any extra adder's.
   */
  function _cleanupFocus($elem) {
    // Remove extra adder's that don't belong anymore.
    // if ($elem.hasClass('SCRBBLER-scribble-adder')) {
    //  var $parent = $elem.parent();
    //  var value = $elem.find('.SCRBBLER-scribble-name').text();
    //  if (value === "") {
    //    var $adderList = $parent.children('.SCRBBLER-scribble-adder');
    //    for (var i = 0; i < $adderList.length - 1; ++i) {
    //      $($adderList[i]).remove();
    //    }
    //  }
    //  _enableElementDragDrop($elem.parent());
    // }

    if ($elem.hasClass('SCRBBLER-scribble-adder') || $elem.hasClass('SCRBBLER-scribble')) {
      var $parent = $elem.parent();

      // Remove all adders from the scope and then append a new one to the end.
      var $adderList = $parent.children('.SCRBBLER-scribble-adder');
      var isEditing = false;
      $adderList.each(function() {
        if ($(this).find('.SCRBBLER-scribble-edit').length) {
          isEditing = true;
        }
      });
      $adderList.remove();

      var $scribbleAdder = $(SCRIBBLE_ADDER_TEMPLATE);
      $parent.append($scribbleAdder);
      _createScribbleAdder($scribbleAdder);
      _enableElementDragDrop($parent);

      if (isEditing) {
        $scribbleAdder.click();
      }
    }
  };

  var SCRBBLER = {
    scribbleList: [],
    
    ///////////////////////////////////////////////////////////
    /* Global Public API                                     */
    ///////////////////////////////////////////////////////////

    /**
     * Attach the SCRBBLER editor to an element.
     * Returns the editor object.
     */
    attach: function(elem) {
      return _createEditor(elem);
    },

    /**
     * Detach a SCRBBLER editor from the document.
     * This can either be the element used to attach
     * the editor, or the editor object itself.
     * Also passing nothing in will detach all editors.
     */
    detach: function(elemOrEditor) {
      var editor = undefined;
      if (elemOrEditor instanceof HTMLElement) {
        editor = _findEditor(elemOrEditor).editor;
      } else if (typeof elemOrEditor !== 'undefined') {
        editor = elemOrEditor;
      }

      if (typeof editor === 'undefined') {
        while (_editorList.length) {
          _editorList[0].editor.detach();
        }
        return true;
      } else {
        return editor.detach();
      }

      _lastError = 'Failed to find SCRBBLER editor';
      return false;
    },

    /*
     * Encode the contents of this SCRBBLER editor to an encoded string
     * that can later be used to restore the editor status.
     *
     * Also see decode()
     */
    encode: function(elemOrEditor) {
      var editor = undefined;
      if (elemOrEditor instanceof HTMLElement) {
        editor = _findEditor(elemOrEditor).editor;
      } else if (typeof elemOrEditor !== 'undefined') {
        editor = elemOrEditor;
      }

      if (typeof editor !== 'undefined') {
        return editor.encode();
      }

      _lastError = 'Failed to find SCRBBLER editor';
      return false;
    },

    /**
     * Decodes a previously encoded string and populates the SCRBBLER
     * editor to match.
     *
     * Also see encode()
     */
    decode: function(code) {
      var editor = undefined;
      if (elemOrEditor instanceof HTMLElement) {
        editor = _findEditor(elemOrEditor).editor;
      } else if (typeof elemOrEditor !== 'undefined') {
        editor = elemOrEditor;
      }

      if (typeof editor !== 'undefined') {
        return editor.decode(code);
      }

      _lastError = 'Failed to find SCRBBLER editor';
      return false;
    },

    /**
     * Generate raw javascript code from this SCRBBLER
     */
    script: function() {
      var editor = undefined;
      if (elemOrEditor instanceof HTMLElement) {
        editor = _findEditor(elemOrEditor).editor;
      } else if (typeof elemOrEditor !== 'undefined') {
        editor = elemOrEditor;
      }

      if (typeof editor !== 'undefined') {
        return editor.script();
      }

      _lastError = 'Failed to find SCRBBLER editor';
      return false;
    },

    /**
     * Retrieve the last error message, if any
     */
    getLastError: function() {
      return _lastError;
    },

    /**
     * Sets the indentation to a given string.
     */
    setIndentation: function(indent) {
      _indent = indent;
    }
  };

  return SCRBBLER;
})